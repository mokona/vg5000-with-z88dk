The build is done through the Scons utility (https://scons.org).

To build, on the root folder of the project : `scons`

Artifacts will be placed in the `build/` sub-folder.

Only tested with the snap install of z88dk on Ubuntu for now.
